<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">

            <div class="row">
                <div class="col-sm-12 input-search">
                    <input type="text" class="form-control fnt-awsm" placeholder='&#xf002; Search'
                           aria-describedby="basic-addon1">

                    <button class="btn btn-warning merch-search-bt">Search</button>
                </div>
            </div>
            <ol class="breadcrumb">
                <li><a href="#">Home</a></li>
                <li><a href="#">Library</a></li>
                <li class="active">Data</li>
            </ol>

            <div class="row min-mar">
                <div class="col-sm-3 pad-home-sec">
                    <div class="panel-group" id="accordion_merchant" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingOne">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#cat_merchant"
                                   aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">

                                        Categories

                                    </h4>
                                </a>
                            </div>
                            <!--include merchange category-->
                            <?php
                                include 'includes/merchant_category.php'
                            ?>
                            <!--include merchange category-->
                        </div>

                    </div>
                </div>
                <div class="col-sm-9 merchant-right-sec">

                    <h3 class="mer-in-tit">Sub Categories</h3>
                    <div class="row">

                    </div>
                </div>
            </div>

        </div>
    </div>


<?php
//include footer
include 'includes/footer.php';
?>