<?php
//inclue header and navigation
    include 'includes/header.php';
    include 'includes/navigation.php';
?>

<div class="row">
    <div class="col-sm-12 pad-top-login">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <img src="images/logo.png" class="img-center img-responsive">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <input class="form-control login-txt-bx" placeholder="Name">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <input class="form-control login-txt-bx" placeholder="NIC / Passport Number">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <input class="form-control login-txt-bx" placeholder="Mobile Number">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <input class="form-control login-txt-bx" placeholder="Email">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <input id="checkbox1" type="checkbox" name="checkbox" value="1" checked="checked"><label for="checkbox1"><span></span>Subscriber to SMS promo alerts</label>
                        <input id="checkbox2" type="checkbox" name="checkbox" value="2"><label for="checkbox2"><span></span>Subscribe to Email alerts</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <a href="javascript:void(0)" class="btn btn-default col-xs-12 login-btn">Login</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
