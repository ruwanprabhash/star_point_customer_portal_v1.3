<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="home-heading text-center">Cotton Collection Loyalty Card</h2>
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table class="table table-striped his-table">
                                <thead>
                                    <tr>
                                        <th>DATE</th>
                                        <th>POINTS</th>
                                        <th>TRANSACTION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>12 july 2016</td>
                                        <td>250 Points</td>
                                        <td>Lorem Ipsum dolor sit amet, tempus duls ahegar, venetas emin mauris.</td>
                                    </tr>
                                    <tr>
                                        <td>12 july 2016</td>
                                        <td>250 Points</td>
                                        <td>Lorem Ipsum dolor sit amet, tempus duls ahegar, venetas emin mauris.</td>
                                    </tr>
                                    <tr>
                                        <td>12 july 2016</td>
                                        <td>250 Points</td>
                                        <td>Lorem Ipsum dolor sit amet, tempus duls ahegar, venetas emin mauris.</td>
                                    </tr>
                                    <tr>
                                        <td>12 july 2016</td>
                                        <td>250 Points</td>
                                        <td>Lorem Ipsum dolor sit amet, tempus duls ahegar, venetas emin mauris.</td>
                                    </tr>
                                    <tr>
                                        <td>12 july 2016</td>
                                        <td>250 Points</td>
                                        <td>Lorem Ipsum dolor sit amet, tempus duls ahegar, venetas emin mauris.</td>
                                    </tr>
                                    <tr>
                                        <td>12 july 2016</td>
                                        <td>250 Points</td>
                                        <td>Lorem Ipsum dolor sit amet, tempus duls ahegar, venetas emin mauris.</td>
                                    </tr>
                                    <tr>
                                        <td>12 july 2016</td>
                                        <td>250 Points</td>
                                        <td>Lorem Ipsum dolor sit amet, tempus duls ahegar, venetas emin mauris.</td>
                                    </tr>
                                    <tr>
                                        <td>12 july 2016</td>
                                        <td>250 Points</td>
                                        <td>Lorem Ipsum dolor sit amet, tempus duls ahegar, venetas emin mauris.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php
//include footer
include 'includes/footer.php';
?>