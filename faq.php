<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row para_sec_wrap contact_inner_wrap">
                <h3 class=" col-sm-12 sec_main_title text-center">FAQ</h3>
                <div class="col-sm-12">
                    <ul class="fa-ul faq_list">
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>Is there any additional cost?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>What benefits do I get?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>Services available on Star Points Customer Service Hotline?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>How do I redeem Star Points?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                        </li>
                        <li><i class="fa-li fa fa-star"></i>
                            <p>How can I join Star Points rewarding scheme?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>How can I redeem my Star Points via SMS fot Dialog Bill payments or Prepaid Top-ups?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                    </ul>

                    <ul class="fa-ul faq_list">
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>How do I earn Star Points?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>How can I check my Star Points balance?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>How are Star Points calculated?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>Can I request to reverse/cancel a transaction?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                        <li>
                            <i class="fa-li fa fa-star"></i>
                            <p>What is the validity ?</p>
                            <p class="faq_inst_txt faq_lin_brk">
                                Type Star&#60;space&#62;Pay and SMS it to 141<br>
                                Eg. To redeem 500 points type Star Pay 500 and SMS 141
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/footer.php';
?>