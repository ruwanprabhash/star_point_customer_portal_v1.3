
<div class="row">
    <div class="col-sm-12 header_pg">
        <div class="row">
            <div class="col-xs-10 col-xs-offset-1 pad-fter-sec">
                <div class="row">
                    <div class="col-sm-1">
                        <div class="row page-header-pos">
                            <a href="index.php">
                                <div class="col-sm-1 logo_outer_page">
                                    <div class="col-sm-12 logo_inner_page">
                                        <img src="images/logo.png" class="img-responsive"/>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-11">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid">
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#"></a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                    <ul class="nav navbar-nav navbar-right navi_bar">
                                        <li><a class="active" href="logged_home.php" id="navi_home">Home</a></li>
                                        <!--<li><a href="transactions.php" id="navi_trans">Transactions</a></li>-->
                                        <li><a href="merchant_list.php" id="navi_merchant">Merchants</a></li>
                                        <!--<li><a href="game.php" id="navi_games">Games</a></li>-->
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Support <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Redeem Points</a></li>
                                                <li><a href="#">Donate Points</a></li>
                                                <li><a href="#">Transfer Points</a></li>
                                                <li><a href="transaction_history.php">Transaction History</a></li>
                                                <li><a href="transactions.php">Information Center</a></li>
                                                <li><a href="#">About Us</a></li>
                                                <li><a href="#" data-toggle="modal" data-target="#contact">Contact Us</a></li>
                                            </ul>
                                        </li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">User <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#" data-toggle="modal" data-target="#editProfile"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;&nbsp;Edit Profile</a></li>
                                                <li><a href="#" ><i class="fa fa-sign-out" aria-hidden="true"></i>&nbsp;&nbsp;Log Out</a></li>

                                            </ul>
                                        </li>
                                        <li><a class="login-btn-head" href="#" data-toggle="modal" data-target="#myModal"><i class="fa fa-sign-in" aria-hidden="true"></i>&nbsp;&nbsp;Log In</a></li>

                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>