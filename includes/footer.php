<?php
    include 'includes/popups.php';
?>

<div class="row pad-top-footer">
    <div class="col-sm-12 footer-shadow">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 home-gen-pad">
                <div class="row">
                    <div class="col-sm-8">
                        <h2 class="redeem-head">REDEEM YOUR BILL</h2>
                        <p class="ftr-pra">You can redeem your Star Points via SMS for Dialog Mobile Bill payments or <br/>pre paid top-ups.</p>
                        <p class="ftr-pra"> Type star pay &#60;amount to be redeemed&#62; and SMS it to 141</p>
                        <p class="home-pra">Eg. To redeem 500 points type star pay 500 and SMS to 141</p>
                    </div>
                    <div class="col-sm-4">
                        <h2 class="redeem-head">CONTACT US</h2>
                        <p class="home-pra">Customer Hotline - <span>141</span></p>
                        <p class="home-pra">Partner Assistance - <span>077 456 456</span></p>
                        <p class="foot-tag-social">
                            <a href="#" class="footer-links" target="_blank"> <span class="fa-stack fa-lg"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-facebook fa-stack-1x fa-inverse"></i> </span> </a>
                            <a href="#" class="footer-links" target="_blank"> <span class="fa-stack fa-lg"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-twitter fa-stack-1x fa-inverse"></i> </span> </a>
                            <a href="#" class="footer-links" target="_blank"> <span class="fa-stack fa-lg"> <i class="fa fa-circle fa-stack-2x"></i> <i class="fa fa-youtube-play fa-stack-1x fa-inverse"></i> </span> </a>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-12 footer-clr">
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 footer-pad">
                <p class="text-center white-text">
                    <a href="faq.php" class="white-text">FAQ &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    <a href="javascript:void(0)" class="white-text">Terms and Conditions &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    <a href="aboutus.php" class="white-text">About Us &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>
                    Copyright @ 2017 Star Points. All right reserved
                </p>
            </div>
        </div>
    </div>
</div>
</div></div>
<script src="js/custom.js"></script>
</body>
</html>