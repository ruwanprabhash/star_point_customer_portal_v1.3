<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Star Point</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.rateyo.min.js"></script>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,200,300,400,500,600,700,800" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.linearicons.com/free/1.0.0/icon-font.min.css">
    <link rel="stylesheet" href="css/jquery.rateyo.min.css">
    <link href="css/style.css" rel="stylesheet">
    <link href="css/stylesheet2.css" rel="stylesheet">
    <link href="css/thusi.css" rel="stylesheet">
    <link rel="shortcut icon" href="favicon.png">
    <script>

        $(window).scroll(function() {
            if ($(this).scrollTop() > 100){
                $('.header_pg').addClass("sticky");
                $('.slide-wrap').addClass("sticky2");
            }
            else{
                $('.header_pg').removeClass("sticky");
                $('.slide-wrap').removeClass("sticky2");
            }
        });
    </script>
</head>
<body>

<div class="row under-header_wrap">
    <div class="col-sm-12">

    </div>
</div>

<div class="row under-wrap">
    <div class="col-sm-12">
        <span class="lnr lnr-star rotating"></span>
        <span class="lnr lnr-star rotating2"></span>
        <span class="lnr lnr-star rotating3"></span>
        <span class="lnr lnr-star rotating4"></span>
        <span class="lnr lnr-star rotating5"></span>

        <span class="lnr lnr-star rotating6"></span>
        <span class="lnr lnr-star rotating7"></span>

        <span class="lnr lnr-star rotating8"></span>
        <span class="lnr lnr-star rotating9"></span>
        <span class="lnr lnr-star rotating10"></span>
        <span class="lnr lnr-star rotating11"></span>
        <span class="lnr lnr-star rotating12"></span>
        <span class="lnr lnr-star rotating13"></span>
    </div>
</div>
<div class="row main-wrap">
    <div class="col-sm-12">