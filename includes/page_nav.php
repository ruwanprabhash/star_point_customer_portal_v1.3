<div class="row">
    <div class="col-sm-12">
        <!-- Nav tabs -->
        <div class="row page-nav-back">
            <div class="col-sm-10 col-sm-offset-1">
                <ul class="nav nav-tabs page-nav-no-border" role="tablist">
                    <li role="presentation" class="col-sm-2 page-nav-border">

                        <a class="page-nav-main" href="#chk_balance" aria-controls="chk_balance" role="tab"
                           data-toggle="tab">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <img src="images/balance.png" class="img-center img-responsive" alt="menu1"/>
                                </div>
                                    <div class="col-sm-10 col-sm-offset-1">
                                    <h4 class="text-center page-nav">Check Balance</h4>
                                </div>
                            </div>
                        </a>
                        <div class="arrow"></div>
                    </li>
                    <li role="presentation" class="col-sm-2 page-nav-border">

                        <a class="page-nav-main" href="#redeem_points" aria-controls="redeem_points" role="tab"
                           data-toggle="tab">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <img src="images/doller.png" class="img-center img-responsive" alt="menu2"/>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <h4 class="text-center page-nav">Redeem Points</h4>
                                </div>
                            </div>
                        </a>
                        <div class="arrow"></div>
                    </li>
                    <li role="presentation" class="col-sm-2 page-nav-border"><a class="page-nav-main"
                                                                                href="#donate_points"
                                                                                aria-controls="donate_points" role="tab"
                                                                                data-toggle="tab">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <img src="images/gift.png" class="img-center img-responsive" alt="menu3"/>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <h4 class="text-center page-nav">Donate Points</h4>
                                </div>
                            </div>
                        </a>
                        <div class="arrow"></div>
                    </li>
                    <li role="presentation" class="col-sm-2 page-nav-border"><a class="page-nav-main"
                                                                                href="#transfer_points"
                                                                                aria-controls="transfer_points"
                                                                                role="tab" data-toggle="tab">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <img src="images/transfer.png" class="img-center img-responsive" alt="menu4"/>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <h4 class="text-center page-nav">Transfer Points</h4>
                                </div>
                            </div>
                        </a>
                        <div class="arrow"></div>
                    </li>
                    <li role="presentation" class="col-sm-2 page-nav-border"><a class="page-nav-main" href="#offers"
                                                                                aria-controls="offers" role="tab"
                                                                                data-toggle="tab">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <img src="images/percentage.png" class="img-center img-responsive" alt="menu5"/>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <h4 class="text-center page-nav">Offers</h4>
                                </div>
                            </div>
                        </a>
                        <div class="arrow"></div>
                    </li>
                    <li role="presentation" class="col-sm-2 page-nav-border"><a class="page-nav-main" target="_blank"
                                                                                href="http://www.wow.lk/">
                            <div class="row">
                                <div class="col-sm-4 col-sm-offset-4">
                                    <img src="images/wow.png" class="img-center img-responsive" alt="menu6"/>
                                </div>
                                <div class="col-sm-10 col-sm-offset-1">
                                    <h4 class="text-center page-nav">WOW catalogue</h4>
                                </div>
                            </div>
                        </a>
                        <div class="arrow"></div>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1 page-tab-nav">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="chk_balance">
                        <div class="row">
                            <div class="col-sm-12 pad-redeem">
                                <p class="page-nav-para">Your redeemable balance is <strong>1200 points</strong></p>
                                <p class="page-nav-para">of your total balance of <strong>2500 points</strong> to be
                                    expired <strong>on 10 Nov 2016</strong></p>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="redeem_points">

                        <div class="row redeem_point_sec">
                            <div class="col-sm-12 pad-pay-bulk">
                                <div class="row">
                                    <div class="col-sm-12 text-right">
                                        <a href="#" class="btn btn-default pay-bulk" id="pay-bulk-btn">Pay Bulk</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="row">
                                            <div class="col-sm-6 pad-acc-pay">
                                                <input type="text" class="form-control redeem-frm"
                                                       placeholder="Account / Mobile Number">
                                            </div>
                                            <div class="col-sm-6 pad-acc-pay">
                                                <input type="text" class="form-control redeem-frm" placeholder="Points">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-4 pad-acc-pay">
                                                <a href="#" class="btn-btn-default pay-bulk col-xs-12 text-center">Redeem</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row pay-bulk-sec1 animate-before">
                            <div class="col-sm-12 pad-pay-bulk">
                                <div class="row">
                                    <div class="col-sm-12 pad-back-btn">
                                        <a href="#" class="pay-back" id="back-pay-bulk1">Back</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-2 col-sm-offset-5">
                                        <div class="row">
                                            <div class="col-sm-10 col-sm-offset-1">
                                                <a href="#" class="pay-add-itm">
                                                    <div class="add-itm-div">
                                                        <div class="add-div">
                                                            <h4 class="add-itm-clr"><i class="fa fa-plus fa-2x"
                                                                                       aria-hidden="true"></i></h4>
                                                            <h4 class="add-itm-clr">Add Item</h4>
                                                        </div>
                                                    </div>

                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row pay-bulk-sec2 animate-before">
                            <div class="col-sm-12 pad-pay-bulk">
                                <div class="row">
                                    <div class="col-sm-12 pad-back-btn">
                                        <a href="#" class="pay-back" id="back-pay-bulk2">Back</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="row">
                                            <div class="col-sm-6 pad-acc-pay">
                                                <input type="text" class="form-control redeem-frm"
                                                       placeholder="Account / Mobile Number">
                                            </div>
                                            <div class="col-sm-6 pad-acc-pay">
                                                <input type="text" class="form-control redeem-frm" placeholder="Points">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-4 pad-acc-pay">
                                                <a href="#" id="add-itm-pay"
                                                   class="btn-btn-default pay-bulk col-xs-12 text-center">Add Item</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="row pay-bulk-sec3 animate-before">
                            <div class="col-sm-12 pad-pay-bulk">
                                <div class="row">
                                    <div class="col-sm-6 pad-back-btn">
                                        <a href="#" class="pay-back" id="back-pay-bulk3">Back</a>
                                    </div>
                                    <div class="col-sm-6 text-right">
                                        <a href="#" class="btn btn-default pay-bulk" id="add-last">Add Item</a>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3">
                                        <div class="row">
                                            <div class="col-sm-12 pad-acc-pay">
                                                <div class="col-sm-12 table-responsive">
                                                    <table class="table">
                                                        <thead></thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>077 123 4567</td>
                                                            <td>120 Points</td>
                                                            <td><a href="#" class="btn btn-default btn-delete"><i
                                                                        class="fa fa-trash-o"
                                                                        aria-hidden="true"></i></a></td>
                                                        </tr>
                                                        <tr>
                                                            <td>077 123 4567</td>
                                                            <td>120 Points</td>
                                                            <td><a href="#" class="btn btn-default btn-delete"><i
                                                                        class="fa fa-trash-o"
                                                                        aria-hidden="true"></i></a></td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-4 pad-acc-pay">
                                                <a href="#"
                                                   class="btn-btn-default pay-bulk col-xs-12 text-center">Pay</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div role="tabpanel" class="tab-pane" id="donate_points">
                        <div class="row">
                            <div class="col-sm-12 pad-pay-bulk">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3 pad-donnate">
                                        <div class="row">
                                            <div class="col-sm-6 pad-acc-pay">
                                                <select class="form-control redeem-frm">
                                                    <option>Donate To</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 pad-acc-pay">
                                                <input type="text" class="form-control redeem-frm" placeholder="Points">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-4 pad-acc-pay">
                                                <a href="#" class="btn-btn-default pay-bulk col-xs-12 text-center">Donate</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="transfer_points">
                        <div class="row">
                            <div class="col-sm-12 pad-pay-bulk">
                                <div class="row">
                                    <div class="col-sm-6 col-sm-offset-3 pad-donnate">
                                        <div class="row">
                                            <div class="col-sm-6 pad-acc-pay">
                                                <select class="form-control redeem-frm">
                                                    <option>Transfer From</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 pad-acc-pay">

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 pad-acc-pay">
                                                <select class="form-control redeem-frm">
                                                    <option>Transfer To</option>
                                                    <option>2</option>
                                                    <option>3</option>
                                                    <option>4</option>
                                                    <option>5</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-6 pad-acc-pay">
                                                <input type="text" class="form-control redeem-frm"
                                                       placeholder="Account / Mobile Number">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 pad-acc-pay">
                                                <input type="text" class="form-control redeem-frm" placeholder="Points">
                                            </div>
                                            <div class="col-sm-6 pad-acc-pay">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-sm-offset-4 pad-acc-pay">
                                                <a href="#" class="btn-btn-default pay-bulk col-xs-12 text-center">Transfer</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="offers">
                        <div class="row">
                            <div class="col-sm-12 pad-pay-bulk">
                                <div class="row">
                                    <div class="col-sm-10 col-sm-offset-1">
                                        <div class="row">
                                            <div class="col-sm-4 pad-offers">
                                                <div class="row">
                                                    <div class="col-sm-12 offer-header">
                                                        <h3>SEASONAL OFFERS</h3>
                                                    </div>
                                                </div>
                                                <img src="images/offer1.jpg" class="img-responsive" alt="offer1"/>
                                                <div class="col-sm-12 text-right see-more-pad seemore_btn" data-tabID="1">
                                                    <p class="lrn-mr">See More</p>
                                                    <p class="lrn-mr-ic"><i class="fa fa-play-circle"
                                                                            aria-hidden="true"></i></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 pad-offers">
                                                <div class="row">
                                                    <div class="col-sm-12 offer-header">
                                                        <h3>MERCHANT OFFERS</h3>
                                                    </div>
                                                </div>
                                                <img src="images/offer2.jpg" class="img-responsive" alt="offer1"/>
                                                <div class="col-sm-12 text-right see-more-pad seemore_btn" data-tabID="2">
                                                    <p class="lrn-mr">See More</p>
                                                    <p class="lrn-mr-ic"><i class="fa fa-play-circle"
                                                                            aria-hidden="true"></i></p>
                                                </div>
                                            </div>
                                            <div class="col-sm-4 pad-offers">
                                                <div class="row">
                                                    <div class="col-sm-12 offer-header">
                                                        <h3>CV CUSTOMER OFFERS</h3>
                                                    </div>
                                                </div>
                                                <img src="images/offer3.jpg" class="img-responsive" alt="offer1"/>
                                                <div class="col-sm-12 text-right see-more-pad seemore_btn" data-tabID="3">
                                                    <p class="lrn-mr">See More</p>
                                                    <p class="lrn-mr-ic"><i class="fa fa-play-circle"
                                                                            aria-hidden="true"></i></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row min-mar ">
                            <div class="main_wrap">
                                <div class="row">
                                    <div class="col-sm-12 offers-header">
                                        <div class="row">
                                            <div class="btn-group offer_type_navi" role="group">
                                                <button type="button" class="btn btn-default offer-btn-grp">SEASONAL
                                                    OFFERS
                                                </button>
                                                <button type="button" class="btn btn-default offer-btn-grp">MERCHANT
                                                    OFFERS
                                                </button>
                                                <button type="button" class="btn btn-default offer-btn-grp">CV CUSTOMER
                                                    OFFERS
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row offr-row-mar">
                                    <div class="col-sm-3 pad-home-sec">
                                        <div class="panel-group" id="accordion_merchant2" role="tablist" aria-multiselectable="true">
                                            <div class="panel panel-default">

                                                <div class="panel-heading" role="tab" id="headingOne">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#cat_merchant2"
                                                       aria-expanded="true" aria-controls="collapseOne">
                                                        <h4 class="panel-title">

                                                            Collapsible Group Item #1

                                                        </h4>
                                                    </a>
                                                </div>

                                                <div id="cat_merchant2" class="panel-collapse collapse in" role="tabpanel"
                                                     aria-labelledby="headingOne">
                                                    <div class="panel-body">
                                                        <ul class="fa-ul mer-list">
                                                            <li><i class="fa-li fa fa-star"></i>Automobile</li>
                                                            <li><i class="fa-li fa fa-star"></i>Dining</li>
                                                            <li><i class="fa-li fa fa-star"></i>Healthcare & Fitness</li>
                                                            <li><i class="fa-li fa fa-star"></i>Household Needs</li>
                                                            <li><i class="fa-li fa fa-star"></i>Lifestyle</li>
                                                            <li><i class="fa-li fa fa-star"></i>Automobile</li>
                                                            <li><i class="fa-li fa fa-star"></i>Dining</li>
                                                            <li><i class="fa-li fa fa-star"></i>Healthcare & Fitness</li>
                                                            <li><i class="fa-li fa fa-star"></i>Household Needs</li>
                                                            <li><i class="fa-li fa fa-star"></i>Lifestyle</li>
                                                            <li><i class="fa-li fa fa-star"></i>Automobile</li>
                                                            <li><i class="fa-li fa fa-star"></i>Dining</li>
                                                            <li><i class="fa-li fa fa-star"></i>Healthcare & Fitness</li>
                                                            <li><i class="fa-li fa fa-star"></i>Household Needs</li>
                                                            <li><i class="fa-li fa fa-star"></i>Lifestyle</li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="panel panel-default">


                                                <div class="panel-heading" role="tab" id="headingTwo3">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion"
                                                       href="#by_mer_cat2" aria-expanded="false" aria-controls="collapseTwo">
                                                        <h4 class="panel-title">

                                                            Collapsible Group Item #2

                                                        </h4>
                                                    </a>
                                                </div>

                                                <div id="by_mer_cat2" class="panel-collapse collapse" role="tabpanel"
                                                     aria-labelledby="headingTwo">
                                                    <div class="panel-body">
                                                        Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry
                                                        richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor
                                                        brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                                                        aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                                                        Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                        ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer
                                                        farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them
                                                        accusamus labore sustainable VHS.
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- offers tab -->


                                    <div class="col-sm-9 pad-home-sec offers-tab-sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 class="seasonal-offs">SEASONAL OFFERS</h4>
                                                <div class="row mar-offer-sec">
                                                    <div class="col-sm-6 pad-offer-sec">
                                                        <h4 class="mar-bot-ras">25% @ Ranjanas</h4>
                                                        <img src="images/offer1.jpg" class="img-responsive" alt="offer1"/>
                                                        <div class="col-sm-12 pad-offer-sec-para">
                                                            <p class="home-pra">Lorem Ipsum is simply dummy text of the printing
                                                                and typesetting industry. Lorem Ipsum has been the industry's
                                                                standard dummy text ever since the 1500s, when an unknown
                                                                printer took a galley of type and scrambled it to make a type
                                                                specimen book. It has survived not only five centuries, but also
                                                                the leap into electronic typesetting, remaining essentially
                                                                unchanged.</p>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-6 pad-offer-sec">
                                                        <h4 class="mar-bot-ras">Buy 1 Get 1 free @ Thilakawardhana</h4>
                                                        <img src="images/offer2.jpg" class="img-responsive" alt="offer1"/>
                                                        <div class="col-sm-12 pad-offer-sec-para">
                                                            <p class="home-pra">Lorem Ipsum is simply dummy text of the printing
                                                                and typesetting industry. Lorem Ipsum has been the industry's
                                                                standard dummy text ever since the 1500s, when an unknown
                                                                printer took a galley of type and scrambled it to make a type
                                                                specimen book. It has survived not only five centuries, but also
                                                                the leap into electronic typesetting, remaining essentially
                                                                unchanged.</p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-9 pad-home-sec offers-tab-sec">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 class="seasonal-offs">MERCHANT OFFERS</h4>
                                                <div class="row mar-offer-sec">
                                                    <div class="col-sm-6 pad-offer-sec">
                                                        <h4 class="mar-bot-ras">25% @ Ranjanas</h4>
                                                        <img src="images/offer1.jpg" class="img-responsive" alt="offer1"/>
                                                        <div class="col-sm-12 pad-offer-sec-para">
                                                            <p class="home-pra">Lorem Ipsum is simply dummy text of the printing
                                                                and typesetting industry. Lorem Ipsum has been the industry's
                                                                standard dummy text ever since the 1500s, when an unknown
                                                                printer took a galley of type and scrambled it to make a type
                                                                specimen book. It has survived not only five centuries, but also
                                                                the leap into electronic typesetting, remaining essentially
                                                                unchanged.</p>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-6 pad-offer-sec">
                                                        <h4 class="mar-bot-ras">Buy 1 Get 1 free @ Thilakawardhana</h4>
                                                        <img src="images/offer2.jpg" class="img-responsive" alt="offer1"/>
                                                        <div class="col-sm-12 pad-offer-sec-para">
                                                            <p class="home-pra">Lorem Ipsum is simply dummy text of the printing
                                                                and typesetting industry. Lorem Ipsum has been the industry's
                                                                standard dummy text ever since the 1500s, when an unknown
                                                                printer took a galley of type and scrambled it to make a type
                                                                specimen book. It has survived not only five centuries, but also
                                                                the leap into electronic typesetting, remaining essentially
                                                                unchanged.</p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-sm-9 pad-home-sec offers-tab-sec offers_tab_secs">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <h4 class="seasonal-offs">CV CUSTOMER OFFERS</h4>
                                                <div class="row mar-offer-sec">
                                                    <div class="col-sm-6 pad-offer-sec">
                                                        <h4 class="mar-bot-ras">25% @ Ranjanas</h4>
                                                        <img src="images/offer1.jpg" class="img-responsive" alt="offer1"/>
                                                        <div class="col-sm-12 pad-offer-sec-para">
                                                            <p class="home-pra">Lorem Ipsum is simply dummy text of the printing
                                                                and typesetting industry. Lorem Ipsum has been the industry's
                                                                standard dummy text ever since the 1500s, when an unknown
                                                                printer took a galley of type and scrambled it to make a type
                                                                specimen book. It has survived not only five centuries, but also
                                                                the leap into electronic typesetting, remaining essentially
                                                                unchanged.</p>
                                                        </div>

                                                    </div>
                                                    <div class="col-sm-6 pad-offer-sec">
                                                        <h4 class="mar-bot-ras">Buy 1 Get 1 free @ Thilakawardhana</h4>
                                                        <img src="images/offer2.jpg" class="img-responsive" alt="offer1"/>
                                                        <div class="col-sm-12 pad-offer-sec-para">
                                                            <p class="home-pra">Lorem Ipsum is simply dummy text of the printing
                                                                and typesetting industry. Lorem Ipsum has been the industry's
                                                                standard dummy text ever since the 1500s, when an unknown
                                                                printer took a galley of type and scrambled it to make a type
                                                                specimen book. It has survived not only five centuries, but also
                                                                the leap into electronic typesetting, remaining essentially
                                                                unchanged.</p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <!-- end offers tab -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>