<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

    <div class="row">
    <div class="col-sm-10 col-sm-offset-1">
        <div class="row  min-mar">
            <div class="col-sm-6 pad-home-sec">
                <h2 class="home-heading">About Star Points</h2>
                <p class="home-pra">The Star Points network consists of over 400 partner merchants with more than 20,000 partner
                    outlets island wide. They cover a variety of multiple retail sectors expanding to
                    household-items, electronics, grocery, clothing chains, food and beverage, cosmetics, healthcare
                    and domestic and international travel.</p>
                <p class="home-pra">This is the first and only mobile-based loyalty network in the country, With no registration fee,
                    all Dialog Mobile customers can earn Star Points, which are redeemable at a constant face value
                    of Rs. 1 per Point across the entire network.</p>
            </div>
            <div class="col-sm-6 pad-home-sec">
                <h1 class="star-point-mob">Star Points</h1>
                <h1 class="mobile_app">Mobile App</h1>
                <div class="row">
                    <div class="col-sm-12 pad-home-top">
                        <img src="images/home-bnr.png" class="img-responsive">
                        <p class="home-pra pad-home-top">Star Points mobile is an android application exclusively for Dialog mobile subscribers providing access to Star Points services.</p>
                        <div class="col-sm-12 text-right">
                            <p class="lrn-mr">Learn More</p>
                            <p class="lrn-mr-ic"><i class="fa fa-play-circle" aria-hidden="true"></i></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <hr class="home-hr"/>
                <h1 class="star-point-mob text-center">Our Merchants</h1>
            </div>
            <div class="row">
                <div class="col-sm-12 pad-our-merch">
                    <img src="images/client.jpg" class="img-responsive img-center" alt="client"/>
                </div>
            </div>
        </div>

    </div>
</div>


    <div class="row home-bg">
        <span class="lnr lnr-star rotating-mid-1"></span>
        <span class="lnr lnr-star rotating-mid-2"></span>

        <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 home-gen-pad">
            <div class="row mar-home-pad">
                <div class="col-sm-6 pad-home-para2">
                    <h1 class="star-point-mob white-text">Star News</h1>
                    <h2 class="white-text home-bg-tit">The Star Points Loyalty Card joined
                        hands with the Cotton Collection in
                        a landmark</h2>
                    <p class="home-bg-para">The Star Points network consists of over 400 partner merchants with more than 20,000 partner outlets island wide. They cover a variety of multiple retail sectors expanding to household-items, electronics, grocery, clothing chains, food and beverage, cosmetics, healthcare and domestic and international travel.</p>

                    <p class="home-bg-para">This is the first and only mobile-based loyalty network in the country, With no registration fee, all Dialog Mobile customers can earn Star Points, which are redeemable at a constant face value of Rs. 1 per Point across the entire network.</p>
                    <div class="col-sm-12">
                        <p class="lrn-mr white-text home-bg-para">View All</p>
                        <p class="lrn-mr-ic"><i class="fa fa-play-circle" aria-hidden="true"></i></p>
                    </div>
                </div>
                <div class="col-sm-1"></div>
                <div class="col-sm-5 pad-home-para2">

                    <div class="row">
                        <div class="col-sm-12 pad-home-top">
                            <img src="images/mid.jpg" class="img-responsive">


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 home-gen-pad">
            <h1 class="star-point-mob text-center offers-heading">Offers</h1>
            <div class="row row-mar-offers">
                <div class="col-sm-4 pad-offer">
                    <img src="images/offer1.jpg" class="img-responsive img-center">
                </div>
                <div class="col-sm-4 pad-offer">
                    <img src="images/offer2.jpg" class="img-responsive img-center">
                </div>
                <div class="col-sm-4 pad-offer">
                    <img src="images/offer3.jpg" class="img-responsive img-center">
                </div>
            </div>
            <div class="col-sm-12 text-right">
                <p class="lrn-mr">See More</p>
                <p class="lrn-mr-ic"><i class="fa fa-play-circle" aria-hidden="true"></i></p>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/footer.php';
?>