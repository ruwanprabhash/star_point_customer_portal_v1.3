<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row para_sec_wrap contact_inner_wrap">
                <h3 class=" col-sm-12 sec_main_title text-center">Contact Us</h3>
                <div class="col-sm-6">
                    <div>
                        <div class="form-group">
                            <input type="text" class="form-control input_styles" id="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control input_styles" id="email" placeholder="Email">
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control input_styles" id="mobile" placeholder="Mobile Number">
                        </div>
                        <div class="form-group">
                            <textarea  class="form-control input_styles" id="message" placeholder="Message" rows="5"></textarea>
                        </div>
                        <button class="contac_submit">Submit</button>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="abt_us_bg">
                        <img src="images/contactus.jpg" class="img-responsive">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/footer.php';
?>