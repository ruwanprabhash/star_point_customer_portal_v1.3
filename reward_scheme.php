<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row para_sec_wrap">
                <div class="col-sm-3 abt_us_bg">
                    <img src="images/reward-scheme.jpg" class="img-responsive pad-img-hsbc">
                </div>
                <h3 class="sec_main_title">HSBC Reward Scheme</h3>
                <p class="sec_para">In a ground-breaking partnership between two industry heavyweights, global
                    banking giant, HSBC, and Dialog Axiata PLC, Sri Lanka’s largest mobile service provider, have
                    joined hands to enable their clientèle to share the benefits of their respective customer
                    loyalty schemes.
                    <br><br>
                    This unique agreement, the first in a series of offers between the two companies, gives
                    customers the opportunity to switch loyalty schemes based on their convenience. Accordingly, now
                    all Dialog Mobile customers with an HSBC Credit Card would be able to use Star Points to
                    purchase products and services at HSBC Rewards Merchants such as Odel, Cargills, etc, which are
                    not part of the Star Points merchant network. Similarly, HSBC customers could now benefit from
                    exclusive offers from 400-plus Star Points merchants and over 1,000 outlets island-wide through
                    this innovative customer conversion mechanism.
                    <br><br>
                    To transfer from Star Points to HSBC Rewards, Star Points customers need to type their credit
                    card number and the amount of Star Points to be transferred and send it to 141
                </p>

                <p class="sec_para">In a ground-breaking partnership between two industry heavyweights, global
                    banking giant, HSBC, and Dialog Axiata PLC, Sri Lanka’s largest mobile service provider, have
                    joined hands to enable their clientèle to share the benefits of their respective customer
                    loyalty schemes.
                    <br><br>
                    This unique agreement, the first in a series of offers between the two companies, gives
                    customers the opportunity to switch loyalty schemes based on their convenience. Accordingly, now
                    all Dialog Mobile customers with an HSBC Credit Card would be able to use Star Points to
                    purchase products and services at HSBC Rewards Merchants such as Odel, Cargills, etc, which are
                    not part of the Star Points merchant network. Similarly, HSBC customers could now benefit from
                    exclusive offers from 400-plus Star Points merchants and over 1,000 outlets island-wide through
                    this innovative customer conversion mechanism.
                    <br><br>
                    To transfer from Star Points to HSBC Rewards, Star Points customers need to type their credit
                    card number and the amount of Star Points to be transferred and send it to 141
                </p>
                <p class="sec_para eg-color">eg : HSBC < credit card number>
                    <amount to transfer> and send to 141
                </p>
                <p class="sec_para">
                    For transfer from HSBC Rewards to Star Points, customers need to call the HSBC hotline on 011 44
                    72200 and request for the transfer of Rewards points to Star Points.
                    <br><br>
                    HSBC or Star Points customer requests to transfer points between the two loyalty schemes will take
                    approximately 02 working days to process.
                </p>
            </div>
            <div class="row">
                <div class="col-sm-9 pad-trans">
                    <h2 class="trans-heading">Transfer Ratio</h2>
                    <div class="row">
                        <div class="col-sm-10 ">
                            <div class="row trans-rat-div">
                                <div class="col-sm-6">
                                    <p class="ratio-para">Star Points to HSBC Rewards</p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="ratio-para">1 Star Point = 1 HSBC Reward</p>
                                </div>
                            </div>
                            <div class="row trans-rat-div">
                                <div class="col-sm-6">
                                    <p class="ratio-para">HSBC to Star Points</p>
                                </div>
                                <div class="col-sm-6">
                                    <p class="ratio-para">3 HSBC rewards = 1 Star Point</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 pad-trans">

                </div>
            </div>
            <div class="row">
                <div class="col-sm-9 pad-trans">
                    <h2 class="trans-heading">Transfer Ratio</h2>
                    <div class="row">
                        <div class="col-sm-10 ">
                            <p class="sec_para">
                                HSBC Rewards to Dialog Star Points – Minimum 100 HSBC Rewards and above
                                <br><br>
                                Dialog Star Points to HSBC Rewards – Minimum 100 Dialog Star Points and above
                                (minimum accumulated balance of 200 Star Points to be eligible)
                                <br><br>
                                To know about HSBC rewards visit: <a href="http://www.hsbc.lk/"> www.hsbc.lk</a>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 pad-trans">
                    <img src="images/gifts.png" class="img-responsive">
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/footer.php';
?>