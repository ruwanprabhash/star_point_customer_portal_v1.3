<?php
//inclue header and navigation
    include 'includes/header.php';
    include 'includes/navigation.php';
?>

<div class="row">
    <div class="col-sm-12 pad-top-login">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <img src="images/logo.png" class="img-center img-responsive">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <input class="form-control login-txt-bx" placeholder="Enter Code">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <a href="home.php" class="btn btn-default col-xs-12 login-btn">Verify</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <h4 class="purple-text text-center heading-mar0"><a href="register.php"><span class="yellow-text">Change Number</span></a></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-reg">
                        <h4 class="purple-text text-center">DIDN'T GET THE CODE? <a href="#"><span class="yellow-text">RESEND SMS</span></a></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>