<?php
//inclue header and navigation
    include 'includes/header.php';
    include 'includes/navigation.php';
?>

<div class="row">
    <div class="col-sm-12 pad-top-login">
        <div class="row">
            <div class="col-sm-4 col-sm-offset-4">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-4">
                        <img src="images/logo.png" class="img-center img-responsive">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <input class="form-control login-txt-bx" placeholder="Mobile Number">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-login">
                        <a href="verify.php" class="btn btn-default col-xs-12 login-btn">Login</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 pad-top-txt-reg">
                        <h4 class="purple-text text-center">NEW TO STAR POINT? <a href="register.php"><span class="yellow-text">REGISTER</span></a></h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

