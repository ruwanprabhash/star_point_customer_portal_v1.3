<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row para_sec_wrap">
                <h3 class="sec_main_title">Cargills Member Loyalty Card</h3>
                <img src="images/reward-scheme-2.jpg" class="img-responsive pad-img-hsbc2">
                <p class="sec_para">The Star Points Loyalty Card joined hands with the Cargills FoodCity in a landmark
                    partnership to offer all Sri Lankan consumers a one-of-a-kind lifestyle ‘loyalty’ experience through
                    its ‘Cargills Member’ Loyalty Card. The coming together of Cargills FoodCity, Sri Lanka’s largest
                    supermarket chain and the country’s largest loyalty network Star Points will create a holistic
                    lifestyle experience for the individual shopper.
                    <br><br>
                    The Cargills Member Cardholders are entitled to a wide array of exclusive benefits throughout the
                    year. The loyalty card will be another access mode for the mobile based Star Points account.
                </p>
                <h2 class="trans-heading">Be a member of the Cargills Member Loyalty Card</h2>

                <p class="sec_para">The card is issued free to any customer who simply has to fill in a form of
                    registration and hand it over to the cashier to receive the Cargills Member Loyalty Card
                    over-the-counter. Based on the points accumulated the membership would be tiered into Gold, Silver
                    and Bronze cardholders.
                    <br><br>
                    All you should do is shop for above Rs. 500 and be eligible for the Card.
                </p>
                <h2 class="trans-heading">Earn Star Points through Cargills Member Loyalty Card</h2>
                <ul class="fa-ul loyal-ul">
                    <li><i class="fa-li fa fa-star"></i>You earn points each time you shop for above Rs. 100.00 at any
                        Cargills FoodCity and Express Outlets island wide.
                    </li>
                    <li><i class="fa-li fa fa-star"></i>You earn minimum of 1 Star Point for each Rs.1000.00 spent at
                        Cargills FoodCity and Express Outlets.
                    </li>
                    <li><i class="fa-li fa fa-star"></i>Every point earned is equivalent to Rs. 1.00.
                    </li>
                </ul>

                <h2 class="trans-heading">Redeem Star Points through Cargills Member Loyalty Card</h2>
                <ul class="fa-ul loyal-ul">
                    <li><i class="fa-li fa fa-star"></i>You can redeem points at any Cargills FoodCity, Express Outlets
                        or any Star Partner Merchant Outlet throughout the year.
                    </li>
                    <li><i class="fa-li fa fa-star"></i>A minimum account balance of 100 points should be maintained at
                        all times.
                    </li>
                    <li><i class="fa-li fa fa-star"></i>You must provide your mobile number in order to redeem at other
                        Star Partner outlets. It is mandatory to provide information to questions (based on your NIC)
                        asked by the Cashier to complete the transaction.
                    </li>
                </ul>

                <h2 class="trans-heading">Check your Star Points balance</h2>
                <p class="sec_para">If you are a Dialog Mobile Customer, following options are available on your
                    device.</p>
                <ol class="point-blnce">
                    <li>Dial #141# to check the total redeemable balance.</li>
                    <li>Request Cargills FoodCity Cashiers to check your balance by giving your Cargills Member Card.
                    </li>
                    <li>SMS – Type Star Points and send to 141</li>
                    <li>Dial 141 Star Points Customer support.</li>
                </ol>
                <p class="sec_para">
                    Non-Dialog Customers can visit any Cargills Food City or Express Outlet and check the points by
                    producing the Cargills Member Card.
                    <br><br>
                    For more information, please call Cargills on 0777 181 181 (8.15 a.m. to 6.30 p.m.) or visit any
                    Cargills FoodCity or Express outlets or log on to www.cargillsceylon.com/cargillsloyalty.
                </p>


            </div>


        </div>
    </div>

<?php
//include footer
include 'includes/footer.php';
?>