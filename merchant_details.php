<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">

            <div class="row">
                <div class="col-sm-12 input-search">
                    <input type="text" class="form-control fnt-awsm" placeholder='&#xf002; Search'
                           aria-describedby="basic-addon1">

                    <button class="btn btn-warning merch-search-bt">Search</button>
                </div>
            </div>


            <div class="row min-mar">
                <div class="col-sm-3 pad-home-sec">
                    <div class="panel-group" id="accordion_merchant" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default">

                            <div class="panel-heading" role="tab" id="headingOne">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#cat_merchant"
                                   aria-expanded="true" aria-controls="collapseOne">
                                    <h4 class="panel-title">

                                        Collapsible Group Item #1

                                    </h4>
                                </a>
                            </div>

                            <!--include merchange category-->
                            <?php
                            include 'includes/merchant_category.php'
                            ?>
                            <!--include merchange category-->
                        </div>

                    </div>
                </div>
                <div class="col-sm-9 pad-home-sec">
                    <h4 class="seasonal-offs">MERCHANT DETAILS</h4>
                    <div class="row">
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <a href="#">
                                <div class="row branch-box">
                                    <div class="col-xs-3 text-center">
                                        <img src="images/mer3.jpg" class="img-responsive">
                                    </div>
                                    <div class="col-xs-9 text-left branch-in-text merch_det_pad">
                                        <h4>Abstract Pannipitiya </h4>
                                        <h5>770, Pannipitiya Rd</h5>
                                        <div class="merchant_det_rate">
                                            <div>Ratings 7.8/10</div>
                                            <div id="rateYo1" class=""></div>
                                        </div>
                                    </div>

                                </div>
                            </a>
                        </div>
                        <div class="col-sm-6 col-md-6 col-lg-4">
                            <div class="merch_det_acctions">
                                <h3>
                                    <i class="fa fa-star" aria-hidden="true"></i><br>
                                    Rate
                                </h3>
                                <h3>
                                    <i class="fa fa-globe" aria-hidden="true"></i><br>
                                    Website
                                </h3>
                                <h3>
                                    <i class="fa fa-location-arrow" aria-hidden="true"></i><br>
                                    Direction
                                </h3>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">

                        </div>
                        <div class="col-sm-6">

                        </div>
                    </div>
                    <h4 class="seasonal-offs">COMMENTS</h4>
                    <div class="row">
                        <div class="col-sm-12 user-sec">
                            <h4>User 1</h4>
                            <p class="user-para">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has
                                survived not only five centuries.</p>
                            <p class="user-date">13/06/2017 05.23 PM</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 user-sec">
                            <h4>User 2</h4>
                            <p class="user-para">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum
                                has been the industry's standard dummy text ever since the 1500s, when an unknown
                                printer took a galley of type and scrambled it to make a type specimen book. It has
                                survived not only five centuries.</p>
                            <p class="user-date">13/06/2017 05.23 PM</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">
                            <select class="form-control">
                                <option>Anonymous</option>
                                <option>2</option>
                                <option>3</option>
                                <option>4</option>
                                <option>5</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 pad-usr-com">
                            <textarea class="form-control usr-cmnt-txt" placeholder="Type your comment"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 pad-usr-com text-right">
                            <a href="#" class="btn-btn-default pay-bulk text-center">Comment</a>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


<?php
//include footer
include 'includes/footer.php';
?>