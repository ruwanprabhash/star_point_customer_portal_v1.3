<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

<div class="row">
  <div class="col-sm-10 col-sm-offset-1">
        <div class="row para_sec_wrap">
            <div class="col-sm-3 abt_us_bg">
                <img src="images/aboutus_img.jpg">
            </div>
            <div class="col-sm-9 para_txt_wrap">
                <h3 class="sec_main_title">About Us</h3>
                <p class="sec_para">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                    In libero eros, tincidunt in lacinia ut, luctus ut enim. Integer id sem convallis, finibus arcu placerat, fringilla velit.
                    Maecenas molestie lacus gravida felis posuere mattis. Etiam quis bibendum arcu.
                    Proin volutpat purus est, sit amet ultricies nibh pellentesque in.
                    Ut vel quam ac urna pretium ornare. Donec vel risus euismod urna maximus pellentesque.
                    Morbi ullamcorper ex sit amet malesuada commodo. Nullam dapibus sem lorem, nec sollicitudin risus ultricies ac.
                    <br><br>
                    Suspendisse interdum est vel luctus mattis.
                    Fusce rhoncus consectetur sapien fermentum sagittis.
                    Ut maximus sem quis nisi malesuada, vitae condimentum lectus commodo.
                    Sed nibh arcu, dapibus nec enim ut, facilisis ullamcorper risus.
                    Suspendisse sodales turpis ut pellentesque consequat.
                    Vivamus et tristique urna, non dictum libero. Nunc consequat aliquet est, vel tincidunt mauris auctor eget.
                    Aenean semper augue id vulputate elementum. Duis a laoreet eros.
                </p>
            </div>
        </div>
  </div>
</div>

<?php
//include footer
include 'includes/footer.php';
?>