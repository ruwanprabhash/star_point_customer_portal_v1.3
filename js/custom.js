$("#pay-bulk-btn").click(function(){
    $(".redeem_point_sec").hide();
    $(".pay-bulk-sec1").show();
    $('.pay-bulk-sec1').addClass('animate_pay');
});
$("#back-pay-bulk1").click(function(){
    $(".pay-bulk-sec1").hide();
    $(".redeem_point_sec").show();
    $('.redeem_point_sec').addClass('animate_pay');
});
$(".add-itm-div").click(function(){
    $(".pay-bulk-sec1").hide();
    $(".pay-bulk-sec2").show();
    $('.pay-bulk-sec2').addClass('animate_pay');
});
$("#back-pay-bulk2").click(function(){
    $(".pay-bulk-sec2").hide();
    $(".pay-bulk-sec1").show();
    $('.pay-bulk-sec1').addClass('animate_pay');
});
$("#add-itm-pay").click(function(){
    $(".pay-bulk-sec2").hide();
    $(".pay-bulk-sec3").show();
    $('.pay-bulk-sec3').addClass('animate_pay');
});
$("#add-last").click(function(){
    $(".pay-bulk-sec1").hide();
    $(".pay-bulk-sec3").hide();
    $(".pay-bulk-sec2").show();
    $('.pay-bulk-sec2').addClass('animate_pay');
});

$(document).ready(function () {
    //faq list show hide toggle
    $(".faq_list li").on("click", function () {
        var currentEle = $(this).find(".faq_inst_txt");
        if($(this).is(".active")){
            $(this).removeClass("active");
            currentEle.slideUp();
        }
        else{
            $(this).addClass("active");
            currentEle.slideDown();
            $(".faq_inst_txt").not(currentEle).slideUp();
            $(".faq_list li").not(this).removeClass("active");
        }
    });
    
    //offer show hide tabs
    $(".seemore_btn").on("click", function () {
        var currentIndex = $(this).attr("data-tabID");
        $(".main_wrap").slideDown();
        showTabAndActive(currentIndex-1);
        var currentTab_cont = $(".offers-tab-sec").eq(currentIndex-1);
        currentTab_cont.fadeIn(200);
        $(".offers-tab-sec").not(currentTab_cont).fadeOut(200);
    });

    function showTabAndActive(eleIndex) {
        var clickedType = $(".offer_type_navi button").eq(eleIndex);
        clickedType.addClass("active");
        $(".offer_type_navi button").not(clickedType).removeClass("active");
    }

    $(".offer_type_navi button").on("click", function () {
        $(this).addClass("active");
        $(".offer_type_navi button").not(this).removeClass("active");
        var clickedIndex = $(this).index();
        console.log(clickedIndex);
        var relTab = $(".offers-tab-sec").eq(clickedIndex);
        relTab.fadeIn(200);
        $(".offers-tab-sec").not(relTab).fadeOut(200);

    });

    //rateyo scripts
    $(function () {

        $("#rateYo1").rateYo({
            starWidth: "15px",
            normalFill: "#eac8f5",
            rating: 4,
            ratedFill: "#b04ad2",
            spacing: "1px",
            readOnly: true
        });

    });

});