<?php
//inclue header and navigation
include 'includes/header.php';
include 'includes/navigation.php';
?>

    <div class="row">
        <div class="col-sm-12">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <div class="item active">
                        <img src="images/slide1.jpg" alt="slider1">
                    </div>
                    <div class="item">
                        <img src="images/slide2.jpg" alt="slider2">
                    </div>
                    <div class="item">
                        <img src="images/slide3.jpg" alt="slider3">
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php
//include footer
include 'includes/page_nav.php';
?>

    <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
            <div class="row">
                <div class="col-sm-6 pad-home-sec">
                    <img src="images/cotton.jpg" class="img-responsive">
                </div>
                <div class="col-sm-6 pad-home-sec">
                    <h2 class="home-heading">Cotton Collection <br>Loyalty Card</h2>
                    <p class="home-pra">The Star Points Loyalty Card joined hands with the Cotton Collection in a
                        landmark partnership to offer all the fashion lovers a one-of-a-kind lifestyle ‘loyalty’
                        experience through its Cotton Collection Loyalty Card.</p>
                    <p class="home-pra">Shoppers can now register to a personalized loyalty experience and be rewarded
                        with redeemable loyalty points, exclusive promotions and unbelievable discounts. The loyalty
                        card will offer customers a chance to collect and redeem Star Points on all purchases at cotton
                        collection outlets</p>
                </div>

            </div>

        </div>
    </div>

<?php
//include footer
include 'includes/footer.php';
?>